import csv
from collections import defaultdict

italy2008file = '../../data/italy-fruili-region/v1_08_r6.txt'
italy2010file = '../../data/italy-fruili-region/v2_10_r6.txt'
#gibbs100krunfile  = '../italyHousing.txt'
gibbs100krunfile = '../lambda-EditDistance_0.001_9.999-20150319-235343.txt'

entries = {}
i8entries = {}
i10entries = {}
with open(italy2008file, 'r') as i8:
    i8reader = csv.reader(i8, delimiter=' ')
    idx = 0 
    next(i8reader)
    for entry in i8reader:
        entries[idx] = entry
        i8entries[entry[0]] = entry
        idx += 1

with open(italy2010file, 'r') as i10:
    i10reader = csv.reader(i10, delimiter=' ') 
    next(i10reader)
    for entry in i10reader:
        entries[idx] = entry
        i10entries[entry[0]] = entry
        idx += 1
print("last idx {0}".format(idx))
samples = []

truth = []

for key in i8entries:
    if key in i10entries: 
        truth.append((i8entries[key], i10entries[key]))

print("number of true matches: {0}".format(len(truth)))

with open(gibbs100krunfile, 'r') as gibbs:
    gibbsreader = csv.reader(gibbs, delimiter=' ')
    next(gibbsreader)
    for sample in gibbsreader:
        samples.append(sample)

#now, take the last sample, and see which entries matched

last = samples[-1]
matches = defaultdict(list)
for index, entity in enumerate(last):
    idx = index
    if idx in entries:
        currEntity = entries[idx]
        matches[entity].append(currEntity)

acceptedMatches = []
for match in matches:
    currMatch = matches[match]
    if len(currMatch) >= 2:
        acceptedMatches.append(currMatch)

#next, let's see if there are certain bins that correspond to a match
#this probably isn't the perfect way to do this, but it's what I came up with
sex = defaultdict(int)
year = defaultdict(int)
studio = defaultdict(int)
qual = defaultdict(int)
sett = defaultdict(int)

sext = defaultdict(int)
yeart = defaultdict(int)
studiot = defaultdict(int)
qualt = defaultdict(int)
settt = defaultdict(int)

sexf = defaultdict(int)
yearf = defaultdict(int)
studiof = defaultdict(int)
qualf = defaultdict(int)
settf = defaultdict(int)

def add(m, n, s, y, st, q, stt):
    s[build_str(m[1], n[1])] += 1            
    if m[2] == n[2]:
        y["same"] += 1
    else: 
        y["diff"] += 1
    #y[build_str(m[2], n[2])] += 1
    st[build_str(m[3], n[3])] += 1
    q[build_str(m[4], n[4])] += 1
    stt[build_str(m[5], n[5])] += 1
    

def build_str(val1, val2):
    return "{0}:{1}".format(val1, val2)

#print(acceptedMatches)
finalAccepted = []
numFalse = 0
for match in acceptedMatches:
    for i, m in enumerate(match):
        for j, n in enumerate(match):
            if i >= j:
                continue
            add(m, n, sex, year, studio, qual, sett)
            if m[0] == n[0]:
                print("{0}, {1}".format(m[0], n[0]))
                add(m, n, sext, yeart, studiot, qualt, settt)
                finalAccepted.append((m, n))
            else:
                add(m, n, sexf, yearf, studiof, qualf, settf)
                numFalse += 1
                print("{0}, {1}".format(m[0], n[0]))
print("false Accepted: {0}".format(numFalse))
print("num disc true {0}".format(len(finalAccepted)))
print("Total {0}".format(len(acceptedMatches)))
print("==========================================")
print("ALL=================")
print("Sex: {0}".format(sex))            
print("Year: {0}".format(year))
print("Studio: {0}".format(studio))
print("Qual: {0}".format(qual))
print("Sett: {0}".format(sett))

print("TRUE================")
print("Sex: {0}".format(sext))            
print("Year: {0}".format(yeart))
print("Studio: {0}".format(studiot))
print("Qual: {0}".format(qualt))
print("Sett: {0}".format(settt))

print("FALSE================")
print("Sex: {0}".format(sexf))            
print("Year: {0}".format(yearf))
print("Studio: {0}".format(studiof))
print("Qual: {0}".format(qualf))
print("Sett: {0}".format(settf))
