---
title: "R Notebook"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

```{r, echo=TRUE, message=FALSE}
library(RecordLinkage)
data("RLdata500")
head(RLdata500)
```
```{r}
X.s <- RLdata500[-c(2, 4)]
Z.s <- RLdata500[-c(2, 4)]
p.s <- ncol(X.s)
#r.fname <- as.matrix(RLdata500[1])
library(RecordLinkage)
rpairs <- compare.dedup(X.s, strcmp=TRUE)
```
```{r}
subset(rpairs$pairs, id1 == 2 & id2 == 43)
```

```{r}
exact.match <- subset(rpairs$pairs, fname_c1 == 1 & lname_c1 == 1 & by == 1 & bm == 1 & bd == 1)
```
```{r}
neartwin.match <- subset(rpairs$pairs, (fname_c1 == 1 & by == 1 & bm == 1 & bd == 1) | (lname_c1 == 1 & by ==1 & bm == 1 & bd == 1))
```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).
```{r}
library(ebLink)
library(plyr)
linkmod <- function(df) {
  c(df$id1, df$id2)
}
true.links.pair <- links(matrix(identity.RLdata500,nrow=1))
exact.links.pairs <- dlply(exact.match, .(id1, id2), linkmod)
neartwin.links.pairs <- dlply(neartwin.match, .(id1, id2), linkmod)
```
```{r}
comparison <- links.compare(exact.links.pairs,true.links.pair,counts.only=TRUE)
all_compare <- links.compare(exact.links.pairs, true.links.pair, counts.only=FALSE)

write.table(all_compare$correct, "/Users/bwalenz/reclink/bwalenz/exact_correct.csv", sep="|");
write.table(all_compare$incorrect, "/Users/bwalenz/reclink/bwalenz/exact_incorrect.csv", sep="|");
write.table(all_compare$missing, "/Users/bwalenz/reclink/bwalenz/exact_missing.csv", sep="|");
comparison

missing.links <- comparison$missing
true.links<-comparison$correct 
false.links <- comparison$incorrect
truth.links <- true.links+missing.links
fpr = false.links/truth.links
fnr = missing.links/truth.links
cnl = 500 * 500 - 50 - 450
fdr = false.links/(cnl+false.links)
fnr
fdr
```
```{r}
comparison <- links.compare(neartwin.links.pairs,true.links.pair,counts.only=TRUE)
all_compare <- links.compare(neartwin.links.pairs, true.links.pair, counts.only=FALSE)
write.table(all_compare$correct, "/Users/bwalenz/reclink/bwalenz/neartwin_correct.csv", sep="|");
write.table(all_compare$incorrect, "/Users/bwalenz/reclink/bwalenz/neartwin_incorrect.csv", sep="|");
write.table(all_compare$missing, "/Users/bwalenz/reclink/bwalenz/neartwin_missing.csv", sep="|");

comparison

missing.links <- comparison$missing
true.links<-comparison$correct 
false.links <- comparison$incorrect
truth.links <- true.links+missing.links
fpr = false.links/truth.links
fnr = missing.links/truth.links
cnl = 500 * 500 - 50 - 500
fdr = false.links/(cnl+false.links)
fnr
fdr
```

```{r}
true.links.pair
```

