# RECLINK
record linkage package: plans, proposals, papers, data, code, results

This repository includes work from the [Advancements in Modern Record Linkage, 2016, Duke University, http://www2.stat.duke.edu/~rcs46/linkage.html]

Here is a list of relevant directories:
```
template-directory/ example structure for creating code for students, authors, etc
	needs/  documents (LaTeX, markdown, docx) describing participants' needs for blocking approaches

data/ contains sample data for record linkage/de-duplication

More will be added as this package is developed and expanded.
```

To see more details of how to structure your files please see a great blog post [this blog post](https://hrdag.org/2016/06/14/the-task-is-a-quantum-of-workflow/). This is greatly recommended for workflow. 

In particular, it would be useful if in your participant directory, you included a subdirectory called `output/` in which you report a file mapping record identifiers to cluster labels (perhaps called `output/labels.csv`). This will facilitate us grouping the results.

### Acknowledgement

The Isaac Newton Institute asks that Work arising from this collaboration includes the following:

The author(s) would like to thank the Isaac Newton Institute for the Mathematical Sciences for support and hospitality during the programme *Data Linkage: Techniques, Challenges and Applications (02)*, 12-16 September 2016, when work on this paper was begun. This work was supported by EPSRC Grant Number EP/K032208/1.